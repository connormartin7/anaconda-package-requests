This is a conda util that will allow people to request and approve packages via a web client.

The data is stored in a couchdb and the site is a Flask app

Future state is to put this in docker, right now it runs locally.

Right now get_all is a list of package names, future state could be to make it an accorion. I'd like to see some clickable items but for now we just have a list.
