import requests

def get_couch_dbs():
	couch_dbs = requests.get('http://127.0.0.1:5984/_all_dbs')
	return {"databases" : couch_dbs.json()}

def get_couch_db_info(db_name):
	couch_db = requests.get('http://127.0.0.1:5984/{}'.format(db_name))
	return couch_db.json()

