import requests
import json
import ast
import sys

def get_all_documents(db_name):
	documents = requests.get('http://127.0.0.1:5984/{}/_all_docs'.format(db_name))
	documents = documents.json()
	listy = []
	for doc in documents['rows']:
		listy.append(doc['id'])
	json_payload = {}
	json_payload['keys'] = listy
	all_documents = requests.post('http://127.0.0.1:5984/{}/_all_docs?include_docs=true'.format(db_name), data = json.dumps(json_payload))
	return all_documents.json()

def get_document(db_name, document_id):
        document = requests.get('http://127.0.0.1:5984/{}/{}'.format(db_name, document_id))
        return document.json()

def get_latest_document(db_name):
        document = requests.get('http://127.0.0.1:5984/{}/_changes?descending=true'.format(db_name))
        return document.json()

def put_document(db_name, json_data):
	document = json.dumps(get_latest_document(db_name)) #string because we can't load it as anything else for some reason 
	document = ast.literal_eval(document) #dictionary
	doc_number = int(document['results'][0]['id']) + 1
	requests.put('http://127.0.0.1:5984/{}/{}'.format(db_name, doc_number), data = json_data)
	return "Posted!"
