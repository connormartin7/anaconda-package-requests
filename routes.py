from flask import Flask, request, render_template
import json
import db_service
import document_service
import sys

app = Flask(__name__)

@app.route('/db/get_all')
def get_all_dbs():
	couch_dbs = db_service.get_couch_dbs()
	return json.dumps(couch_dbs)

@app.route('/db/get_info/<db_name>')
def get_couch_db(db_name):
	couch_db = db_service.get_couch_db_info(db_name)
	return json.dumps(couch_db)

@app.route('/document/get_all/<db_name>')
def get_all_documents(db_name):
	documents = document_service.get_all_documents(db_name)
	return render_template("home_page.html", data=json.dumps(documents))

@app.route('/document/get_by_id/<db_name>/<document_id>')
def get_document_by_id(db_name, document_id):
        document = document_service.get_document(db_name, document_id)
        return json.dumps(document)

@app.route('/document/get_latest/<db_name>')
def get_latest_document(db_name):
	document = document_service.get_latest_document(db_name)
	return json.dumps(document)

@app.route('/document/put_document/<db_name>', methods=['PUT'])
def put_document(db_name):
	data = request.data
	print(data, file=sys.stderr)
	return document_service.put_document(db_name, data)






